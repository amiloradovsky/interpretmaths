
#+TITLE: The production function in economics.

* Introduction.

It's going to be harsh... but I think it's needed.

*What:* This short series of notes is aimed to give a more clear and robust, than in the vast majority of sources,
explanation of some of the basic economic concepts; and, in some sense, "white-wash" it's name as a science.\\
*How:* By presenting a freely[fn:license] available piece of example of application of math to the subject.
Because without math being used, the theory lacks any concreteness, and becomes pointless "meta-physical",
or "philosophical", if you like, crap.\\
*Why:* I feel like there is huge need in this, and yet the topic is almost secret:
Mathematical Economics is like unicorn -- everybody heard it exists but nobody have seen it...
And it's despite that Economics is supposed to be the science,
immunizing the society from unwise ideas and tricky political slogans.
So I hope these writings will be helpful, regardless of their quality
(cause the standards are, unfortunately, very low anyways).

In defense of the common way of teaching economics,
it is also worth mentioning that Economics as a science is very young yet:
it's actual age is less than a century!
For comparison, Classical Mechanics, Fluid-, Thermo- and Electro-dynamics are /about twice/ as old!
General Relativity, Quantum Mechanics, and many other cool things
already existed in 1920's, but Linear Programming was invented only in 1940's.
The theory of production chains is a little bit older (1920's),
but the usage of production functions,
which we're going to use through this text, is even younger (at least 1950's).

So the theory I'm trying to present here, as accurate as I can, is only about 50 years old,
no surprise it is not yet as mature and/or well-known as say most of physics.

*Note:* Finally, despite this, maybe somewhat harsh, tone of the intro, I'm looking for a feedback,
especially from those more knowledgeable in Economics in general and Mathematical Economics in particular.
But please don't waste your and my time engaging in various discussions about the role of mathematics:
e.g. that a theory is still a theory even if it isn't formulated with a rigorous math concepts --
I'm not going to accept this kind of criticism...

[fn:license] Maybe CC-BY-ND-SA or GNU FDL?

** Macro- and micro-economics.

The distinction between macro- vs. micro-economics (largely condemned terms) is like design vs. implementation:
macro-economics is concerned with the desirable properties of the economy as whole and achievability,
while micro-economics is concerned with the methods of reaching those properties,
by considering more "low-level" structure of the economy, namely, agent based.

So macro- has more to do with the usage of natural resources, ecological aspects,
and the fact that the Earth is finite.
While micro- has more to do with setting up an appropriate "Rules of the Game".

An interesting fact here: the growth models produced in micro-economics are often /exponential/,
while in macro-economics they are /power functions/. Why?

It is known from analysis that, in the limit, exponential grows faster than power.
And from physics it is known that the rate with which a signal can spread,
and therefore the civilization could ever expand, is limited by the Speed of Light, a power law.
Therefore the exponential growth models are in fact "just" an approximation of the more exact law,
known as logistic or S-curve.

It hints why micro-economics alone, with it's exponential growth models,
is not enough to describe what economics aims to describe. (Despite some common beliefs.)
Economics is /not/ contained in Game Theory.

So here we start with macro-economics, as a more abstract part (although it may be not the best way).

* Macroeconomics.

In this section we'll introduce a concrete algebraic expression for the production function,
as a model of a production process, in the form of a /power function/ (sometimes called Cobb-Duglas,
but I'm not sure this naming is correct).

It may be used to model as "the economy as whole" (i.e. in macro-economics),
to determine the optimal conditions for the economy to function,
and, to some degree, how these conditions may be reached.
As an individual agent(s) production (i.e. in micro-economics).

First we'll consider the case of a single good production. Use it to illustrate some important concepts.
Then extend it to the case of multiple goods production, and see what interesting phenomena arise there.

** The production function and it's optimum.

So let's define the power (production) function, and formulate the /optimization problem/ for it.

Let we have

- an index set (class/type) of resources/production factors $I$ (used to name and count them)
- and a production function $f : {\mathbb{R}_+}^I \to \mathbb{R}_{+}$, which is given by [fn:linear-1]

$$f(x) := A \prod_{i\in{}I} x_i^{\alpha_i}$$

Where $x_i$ are the components of $x$ (the input amounts),
$A: \mathbb{R}_+$ is a proportionality multiplier, and $\alpha: {\mathbb{R}_+}^I$ are the "return coefficients".
Here we use the same coordinate "semi-vector" space of functions $I \to \mathbb{R}_+$ to describe the quantities,
and denote it by ${\mathbb{R}_+}^I$.
And ${\mathbb{R}_+}$ is the real-positive numbers, including zero ($0$ is not neither, it is both, positive and negative).

We wish to find it's /maximum/, s.t. ("subject to") the "budget restrictions",
an /inequality constrains/. I.e. such $t^{*}$ that:

$$f(t^{*}) = \max f(t)$$
$$\sum_{i\in{}I} t_i \le T$$

Here $T: \mathbb{R}_+$ is the (whole) budget, and $t$ are the resources, expressed with a single unit:
which may be a /currency/ or ("the universal currency") /time intervals/.
After all, time is the primary resource, and, in that interpretation,
the problem may be viewed as having to do with the /management/.

First of all, let's assume $A, \alpha_i \neq 0$ for all $i$,
otherwise we may just exclude them from the consideration, and let the corresponding $x_i$ to be $0$.

Since the function $f$ is /not decreasing/ on it's argument
(with respect to the /partial ordering on coordinate vectors/),
it necessary has the maximum when there's /equality/.
I.e. the maximum is on the constraint /surface/.

$$\varphi := T - \sum_{i\in{}I} t_i = 0$$

And these are the new, equivalent, constrains.

To find the /stationary points/, we use the method of Lagrange multipliers.
(I'm not going to explain the details of the method here, it's pure math, and described in many other places.)

Namely, construct the equations from the partial derivatives for each index $i$
of the production function $f$ and the budget restriction $\varphi$,

$$(D_i f)(t) = \alpha_i f(t) t_i^{-1}$$
$$(D_i \varphi)(t) = - 1$$

Using a yet-to-be-determined coefficient $\lambda: \mathbb{R}_{+}$, obtain

$$(D_i f + \lambda D_i \varphi)(t) = \alpha_i f(t) t_i^{-1} - \lambda = 0$$

$$\alpha_i f(t) = \lambda t_i$$

And solve it with the constraint $\varphi = 0$ to get

$$\frac {\alpha_i} {\alpha_j} = \frac {t_i} {t_j}$$

$$\frac {\alpha_i} {\sigma} = \frac {t_i} {T}$$

for any $i, j: I$ and $\sigma := \sum_{i\in{}I}\alpha_i$.
So the stationary point has the coordinates

$$t_i^{*} = T \frac {\alpha_i} {\sigma}$$

And it is indeed the point of maximum,
because on the boundary (when $t_i = 0$ for some $i$) the function $f = 0$,
what's certainly not more than this value.

[fn:linear-1]

*Note:* Alternatively, we might define $f$ as

$$\log f(x) := \log A + \sum_{i\in{}I} \alpha_i \log x_i$$

In this case it is just a linear function, $\log\circ{}f: \mathbb{R}^I \to \mathbb{R}$.

It might be more convenient to operate with addition, instead of multiplication.
Since these two operations are /dual/ to each other, there must be /no difference/ in conclusions,
but easier to work with.

But this form has disadvantage that it's harder to generalize in some directions:
in general case we may need to consider /sum/ of such power functions,
and it'd look not as clean in logarithmic scale.
Also, the cases of $x_i = 0$ or $A = 0$ then become problematic.

** Information measure, entropic component.

Let's introduce the function $\rho: \mathbb{I}^I$,
where $\mathbb{I} := \mathbb{R}\cap{}[0,1]$ is the (closed) unit interval, by

$$\rho_i := \frac {\alpha_i} {\sigma}$$

It is a "probability measure" (to the returns or costs) on $I$, i.e. the measure on the "resource types".
Then use it to get rid of $\alpha$ in the expression for $f$

$$y := f(t) = A \prod_{i\in{}I} t_i^{\sigma\rho_i}$$

When the available budget $T$ is utilized the most efficient way,

$$\rho_i = \frac {t_i} {T}$$

and

$$y = A \prod_{i\in{}I} (T \rho_i)^{\sigma\rho_i}$$

Then

$$y = A \left( T \prod_{i\in{}I} {\rho_i}^{\rho_i} \right)^\sigma$$

An observation.
Besides the obvious parameters on which the output $y$ must depend ($T$, $A$, and $\sigma$),
this expression contains one interesting component:
the dependent product resembles the expression for /entropy/, but inverted and not in logarithmic scale.

It may look a little more obvious and natural, if we use logarithmic scale for $t_i$ and $y/A$:

$${\frac 1 \sigma} \log {\frac y A} = \log T + \sum_{i\in{}I} \rho_i \log \rho_i$$

This equation suggests that the amount of production depends not only on the most obvious parameters:
$T$, $A$, and $\sigma$; but also on the "information" (as negative entropy) contained in the measure $\rho$.
So it may be somehow related to information theory, due to the "entropic component".

** Dynamical systems approach, evolution.

Let's see what happens to such a production being /isolated/.

Introduce some discrete time domain, in form of natural numbers (including zero).
Then suppose $A$ and $\alpha$ do not depend on it explicitly, thus $\sigma$ and $\rho$ too.
Let $T_0$ be the budget at the beginning, and, for any successive cycle $n$, $T_{n+1} = y_n$.

$$T_{n+1} = K T_n^\sigma$$

$$K := A \left( \prod_{i\in{}I} {\rho_i}^{\rho_i} \right)^\sigma$$

To find the explicit expression for $T_n$, consider logarithm of the recursion:

$$\log T_{n+1} = \log K + \sigma \log T_n$$

And the expression for $\log T_n$ is then

$$\log T_n = \sigma^n \log T_0 + \frac {\sigma^n - 1} {\sigma - 1} \log K$$

Exponentiate it back, obtain

$$T_n = K^{\frac {\sigma^n - 1} {\sigma - 1}} T_0^{\sigma^n}$$

Now we can find out what happens to the evolution of production volumes
given different combination of the parameters.

First, suppose $T_0 > 0$ and $K > 0$, otherwise it is uninteresting ($T_n = 0$).
Then consider four cases of how $\sigma$ is related to $0$ and $1$:

- If $\sigma = 0$, then $T_n = K$ for any $n > 0$: no planning is ever needed\dots

- If $\sigma = 1$, then it is just a geometric progression, $T_n = T_0 K^n$:
  the volumes will increase/stay the same/decrease depending on whether $K \gtreqless 1$,
  or $A \gtreqless \left( \prod_{i\in{}I} {\rho_i}^{\rho_i} \right)^{-1}$.

- If $\sigma \in (0, 1)$ or $\sigma \in (1, \infty)$ the situation is more complicated:

Then let's first determine for which $T_0$, if ever, $T_n$ doesn't change, the sequence is constant:
i.e. $T_{n+1} = T_n$, then $T_n = T_0$.

$$T_n = K T_n^\sigma \implies
  T_n^{1-\sigma} = K \implies
  T_n = K^{\frac 1 {1-\sigma}}$$

Let's denote the ("separating") value by $\bar{T}$.

Then what if the value $T_0$ is more/less than that?

$$T_0 \gtrless \bar{T} = K^{\frac 1 {1-\sigma}}$$
$$T_0^{\sigma^n} \gtrless \bar{T}^{\sigma^n}
  = K^{\frac {\sigma^n} {1-\sigma}}$$
$$T_n \gtrless K^{\frac {\sigma^n - 1} {\sigma - 1}} \bar{T}^{\sigma^n}
  = K^{\frac {\sigma^n - 1} {\sigma - 1} + \frac {\sigma^n} {1-\sigma}}
  = K^{\frac 1 {1 - \sigma}} = \bar{T}$$

Therefore

$$T_0 \gtrless \bar{T} \implies T_n \gtrless \bar{T}$$

It means that over time the value $T_n$ always stays above/on/below the "separator" $\bar{T}$.
Moreover, since

$$\frac {T_{n+1}} {T_n} =
  K^{\frac {\sigma^{n+1} - 1} {\sigma - 1} - \frac {\sigma^n - 1} {\sigma - 1}}
  T_0^{\sigma^{n+1} - \sigma^n} =
  K^{\sigma^n} T_0^{\sigma^n (\sigma - 1)} =
  (K T_0^{\sigma - 1})^{\sigma^n}
$$

$$\frac {T_{n+1}} {T_n} = (K T_0^{\sigma - 1})^{\sigma^n} \gtrless 1
\iff K T_0^{\sigma - 1} \gtrless 1$$

$$\frac {T_{n+1}} {T_n} \gtrless 1 \Longleftrightarrow
  (K^{\frac 1 {\sigma - 1}} T_0 \gtrless 1 \land \sigma > 1) \lor
  (K^{\frac 1 {\sigma - 1}} T_0 \lessgtr 1 \land \sigma < 1)
$$

$$T_{n+1} \gtrless T_n \Longleftrightarrow
  (T_0 \gtrless \bar{T} \land \sigma > 1) \lor
  (T_0 \lessgtr \bar{T} \land \sigma < 1)
$$

So, depending on whether $\sigma$ is more/less than 1,
the sequence $T_n$ will go respectively away from or towards the separator $\bar{T}$. I.e. for
- $\sigma < 1$, it sort of "stabilizes", constantly approaching $\bar{T}$ from either direction
- $\sigma > 1$, it is sort of "inhibited" when below $\bar{T}$, and may "grow" only being above that

I.e. there may be two (or four, if you like) types of production processes,
corresponding to different ranges of the coefficient $\sigma$.\\
Since in economics people usually are concerned with growth, and not self-destroyal,
depending on the available starting budget, the production's future is different:
it may or may not be suicidal\dots\\
So the budget should match the parameters.

There is a lot to think about in this distinction: natural monopolies, small businesses etc. etc.
For example, if we assume this model is applicable,
to use an available budget effectively one may either find a production for which

- $\bar{T}$ is greater, but $\sigma < 1$ - slowly approaching the ceiling, common case
- $\bar{T}$ is lower, and $\sigma > 1$ - much brighter perspective, but harder to find

This is an example of analysis of a relatively simple dynamical system.

** Multiple goods production.

Suppose now there are /multiple production processes/ for different goods,
and also suppose the resources distribution may not be varied independently for each production:

$$f: {\mathbb{R}_+}^I \to {\mathbb{R}_+}^I$$

So, $f$ is a map from a coordinate vector of resources to another such vector.
And, analogous to the above case, for each $k: I$, it is given by [fn:linear-n]

$$y_k := f_k(x) := A_k \prod_{i\in{}I} x_i^{\alpha_{k,i}}$$

Here, $f_k\equiv{}0$ means that good $k$ is not produced at all.

[fn:linear-n]

*Note:* Again, in logarithmic scale, it would be

$$\log f_k(x) := \log A_k + \sum_{i\in{}I} \alpha_{k,i} \log x_i$$

A linear transformation, $\log\circ{}f: \mathbb{R}^I \to \mathbb{R}^I$.
Still the same disadvantages.

*** Common stationarity condition.

To find an optimum for this, introduce a "total value" $Y$, to serve the /objective function/

$$Y := F(x) := \sum_{k\in{}I} f_k(x) = \sum_{k\in{}I} y_k$$

Here we suppose that all the proportionality is already included in $A$.
And the constrains are the same as above: for some $X$,

$$\sum_{i\in{}I} x_i \le X$$

Then we seek for a maximum of

$$F(x) = \sum_{k\in{}I} A_k \prod_{i\in{}I} x_i^{\alpha_{k,i}}$$

Under the equality constrains

$$\varphi := X - \sum_{i\in{}I} x_i = 0$$

obtained as above (because $Y$ is non-decreasing for each $x_i$).
Also we know that there's no need to check the boundaries for maximum (there's always $Y = 0$).
Using the same method of Lagrange multipliers, obtain the system

$$(D_i F + \lambda D_i \varphi)(x) = \sum_{k\in{}I} \alpha_{k,i} f_k(x) x_i^{-1} - \lambda = 0$$

$$\sum_{k\in{}I} \alpha_{k,i} f_k(x) = \lambda x_i$$

Since $\alpha: \mathbb{R_+}^{I\times{}I}$ is now a matrix,
introducing $\sigma: \mathbb{R_+}^I$ to be now a (coordinate) vector,

$$\sigma_k := \sum_{i\in{}I} \alpha_{k,i}$$

we also have

$$\sum_{k\in{}I} \sigma_k f_k(x) = \lambda X$$

And here's where matters get dramatically more complex...

Getting rid of \lambda, for any $i, j: I$, we now have

$$\frac {\sum_{k\in{}I} \alpha_{k,i} f_k(x)}
        {\sum_{k\in{}I} \alpha_{k,j} f_k(x)} = \frac {x_i} {x_j}$$

$$\frac {\sum_{k\in{}I} \alpha_{k,i} f_k(x)}
        {\sum_{k\in{}I} \sigma_k f_k(x)} = \frac {x_i} {X}$$

And, we may write the system as follows

$$\sum_{k\in{}I} (\alpha_{k,i} X - \sigma_k x_i) f_k(x) = 0$$

$$\sum_{k\in{}I} \left(
  \frac {\alpha_{k,i}} {\sigma_k} - \frac {x_i} {X}
\right) \sigma_k f_k(x) = 0$$

We then need to solve this system of $|I|$ "not-even-algebraic" equations for $x$ to
find the stationary points... Good news is that we still can derive some properties of it.
And consider some special cases.

*** Rank \alpha \le 1 case.

When $\alpha$ has rank $0$, i.e. $\alpha = 0$, there's nothing to optimize.

When $\alpha$ has rank at most $1$,
i.e. there are such $\beta, \gamma: \mathbb{R_+}^I$, that $\alpha_{k,i} = \beta_k \gamma_i$, then

$$\sigma_k = \sum_{i\in{}I} \beta_k \gamma_i = \beta_k \sum_{i\in{}I} \gamma_i$$

and the stationarity condition is

$$\sum_{k\in{}I} (\alpha_{k,i} X - \sigma_k x_i) y_k = 0$$

$$\sum_{k\in{}I} (\beta_k \gamma_i X - \beta_k \sum_{j\in{}I} \gamma_j x_i) y_k = 0$$

$$(\gamma_i X - \sum_{j\in{}I} \gamma_j x_i) \sum_{k\in{}I} \beta_k y_k = 0$$

Thus, either

$$\sum_{k\in{}I} \beta_k y_k = 0$$

i.e. $y_k = 0$ for any $k$ for which $\beta_k \neq 0$, or

$$\frac {x_i} {X} = \frac {\gamma_i} {\sum_{j\in{}I} \gamma_j} =
  \frac {\alpha_{k,i}} {\sigma_k} =: \rho_{k,i}$$

(which is actually doesn't depend on $k$)

- When $\beta = 1$ for only one $k$, and zero otherwise.
  Then $x$ are optimized only for this single product. This is what we had before.

- Also, when $\beta_k$ may only be $0$ or $1$.
  This is essentially the same setting: multiple products, but reaching optimum simultaneously.

- When $\gamma = 1$ for only one $i$, and zero otherwise.
  Then all the productions depend on only one resource. On which the whole budget is spent.

- Also, when $\gamma_i$ may only be $0$ or $1$.
  There are several resources, of equal impact. And the budget is split equally between them.

So, the case of $\text{rank }\alpha \le 1$ is exhausted. The optimum is known.

**** The entropy.

The production function in the optimum looks like:

$$y_k = A_k \prod_{i\in{}I}
  \left( X \frac {\gamma_i} {\sum_{j\in{}I} \gamma_j} \right)^{\beta_k \gamma_i}
  = A_k \prod_{i\in{}I} \left( X \rho_{k,i} \right)^{\sigma_k \rho_{k,i}}$$

This is exactly the form we had for one good but now parametrized by a $k$.

$$y_k = A_k \left( X \prod_{i\in{}I} \rho_{k,i}^{\rho_{k,i}} \right)^{\sigma_k}$$

So here we also have the "entropic component", which is actually not depending on $k$,
while $\sigma$ and $A$ are. This component is now encoded by $\gamma$.

**** Evolution.

Again, production is isolated; discrete time domain, given by $n: \mathbb{Z}_0$;
$A$ and $\alpha$, thus $\sigma$ and $\rho$, do not depend on time explicitly;
$X_0$ is the start budget and $X_{n+1} = Y_n$ for all $n$.

$$X_{n+1} = \sum_{k\in I} K_k X_n^{\sigma_k}$$

$$K_k = A_k \left( \prod_{i\in{}I} \rho_{k,i}^{\rho_{k,i}} \right)^{\sigma_k}$$

$$\sigma_k = \beta_k \sum_{i\in{}I} \gamma_i$$

We know that even when $\beta_k$, and then $\sigma_k$, doesn't depend on $k$,
the case of essentially single good, the analysis is far from being trivial.
When $\beta_k$ actually does depend on $k$, it may be even more complicated.

Actually, for one particular $\sigma$, when $|I|=2$, we'd have the /logistic difference equation/,
which is known to exhibit /chaotic/ properties.

So, in general, the behavior of this model may be chaotic.
Although, there are techniques to cope with it: such as probabilistic approach,
and approximation of the discrete model with a continuous one.
And, of course, for some special cases the exact solutions may be found.

In general, however, even to find the steady states, where $X_{n+1} = X_n$, it may require to solve

$$X_n = \sum_{k\in I} K_k X_n^{\sigma_k}$$

for $X_n$; and this is hard -- even if $\sigma_k$ are positive integers,
thanks to the fundamental theorem of algebra, the solutions may only be found numerically.

*** Higher rank \alpha (diagonal case).

Suppose $\alpha$ is /diagonal/, i.e. $\alpha_{k,i} \neq 0 \implies k = i$:
each production depends on it's special resource only, so they may be varied independently.
Then $y_k = A_k x_k^{\sigma_k}$ and $\sigma_k = \alpha_{k,k}$.

First we introduce a simple, common, and useful construction -- Production-Possibility Frontier (PPF).

Here we can write down the expression for $x_k$ from $y_k$ explicitly:

$$x_k = \left( \frac {y_k} {A_k} \right)^{\frac 1 {\sigma_k}}
      = A_k^{- \frac 1 {\sigma_k}} y_k^{\frac 1 {\sigma_k}}$$

And substitute it into the constraint $\sum_{i\in{}I} x_i = X$:

$$\boxed{ \sum_{i\in{}I} A_k^{- \frac 1 {\sigma_k}} y_k^{\frac 1 {\sigma_k}} = X }$$

To obtain the implicit function for what's called the Production-Possibility Frontier.
It has the advantage that it no longer contains the factors $x$ in it's expression,
and simply shows what combinations of $y$ may /pretend/ to be the most optimal for /arbitrary/
utility function (given that more is always better, for every product, independently).

Here the region (on or under the surface) plays the role of a constraint
(equality, if only the boundary is considered; and inequality, if the interior isn't excluded yet)
for a utility function. The utility function is linear, in our case,
and to optimize for it one has to solve the same systems of equations,
but where the roles of the equations are reversed (objective/constraint).

The differential [fn:up-index] shows some of the behaviors near the limits,
where some $x_k \to 0$, depending on the respective $\sigma_k$:

$$\sum_{i\in{}I} \left( A_k^{- \frac 1 {\sigma_k}} {\sigma_k^{-1}} \right)
  y_k^{\frac 1 {\sigma_k} - 1} dy_k = 0$$

------------------------

To illustrate the form of PPF, when $|I| = 2$, $X = 1$, all $A_k = 1$,
and each $\sigma_k = 2$ or $\sigma_k = 1/2$ (independently); i.e.

$$y_1 = \left( 1 - y_0^{\frac 1 {\sigma_0}} \right)^{\sigma_1}$$

\includegraphics[page=4]{optimum-2}

------------------------

Now let's get back to the actual computation (again for $x$).
To find the stationary points we have to solve

$$\sum_{k\in{}I} (\alpha_{k,i} X - \sigma_k x_i) y_k = 0$$

$$X \sum_{k\in{}I} \alpha_{k,i} y_k = x_i \sum_{k\in{}I} \sigma_k y_k$$

$$\frac {x_i} {X} = \frac {\sigma_i y_i} {\sum_{k\in{}I} \sigma_k y_k}$$

So, for any $i, j: I$,

$$\sigma_i A_i x_i^{\sigma_i - 1} =
  \frac {\sigma_i y_i} {x_i} = \frac {\sum_{k\in{}I} \sigma_k y_k} {X} =
  \frac {\sigma_j y_j} {x_j} =
  \sigma_j A_j x_j^{\sigma_j - 1}$$

Then, if $\sigma_i \neq 1$,

$$x_i = \left( \frac {\sigma_j A_j} {\sigma_i A_i} \right)^{\frac 1 {\sigma_i - 1}}
  (x_j)^{\frac {\sigma_j - 1} {\sigma_i - 1}}$$

Suppose this is the case for every $i$, so we can express every coefficient $x_i$ with every other $x_j$.
And substitute the expression(s) for $x_i$ into the constraint, $\sum_{i\in{}I} x_i = X$, to obtain

$$\sum_{i\in{}I} \left( \frac {\sigma_j A_j} {\sigma_i A_i} \right)^{\frac 1 {\sigma_i - 1}}
  (x_j)^{\frac {\sigma_j - 1} {\sigma_i - 1}} = X$$

Introducing some new variables,

$$\sum_{i\in{}I} q_{j,i} (x_j)^{p_{j,i}} = X \text{ or }
  \sum_{i\in{}I \land i\ne j} q_{j,i} (x_j)^{p_{j,i}} = X - x_j$$

$$q_{j,i} := \left( \frac {\sigma_j A_j} {\sigma_i A_i} \right)^{\frac 1 {\sigma_i - 1}}
 \text{ and } p_{j,i} := {\frac {\sigma_j - 1} {\sigma_i - 1}}$$

The solution, in general, has to be computed numerically; but before that,
an analysis has to be performed, to determine the qualitative properties of the solution space.
And this is what we'll not do here: just give some rather practical remarks.

------------------------

In the smallest interesting dimension, $|I| = 2$, $i \neq j$, this problem looks like

$$q (x_j)^p + x_j = X \text{ or } q (x_j)^p = X - x_j$$
$$q := \left( \frac {\sigma_j A_j} {\sigma_i A_i} \right)^{\frac 1 {\sigma_i - 1}}
 \text{ and } p := {\frac {\sigma_j - 1} {\sigma_i - 1}}$$

It's the intersection of the line, going through the points $(0, X)$ and $(X, 0)$,
with the power function $x\mapsto qx^p$, where $p$ may be any real number, and $q$ -- any positive.
There is /exactly one/ point of intersection, if $p > 0$;
and, if $p \le 0$, may be /no, one, or two/. To illustrate,

\includegraphics[page=1]{optimum-2}

The problem itself may look like: max./min. of $A x^\alpha + B y^\beta$, s.t. $x + y = 1$. E.g.,

\includegraphics[page=2]{optimum-2}

\includegraphics[page=3]{optimum-2}

------------------------

In general, when $|I| > 2$. The system gives a set of values for every coordinate $x_j$,
and these values for different coordinates somehow correspond to each other.

Using, for instance, the Newton's method, a solution is a root of this function

$$z_j = \sum_{i\in{}I} q_{j,i} (x_j)^{p_{j,i}} - X \text{ then }
 z'_j = \sum_{i\in{}I} q_{j,i} p_{j,i} (x_j)^{p_{j,i} - 1}$$

So a stationary point may be found as the limit(s) of the sequence(s),
given by $x_j^{(0)} := X$ (or $=0$) and the following recursive formula

$$x_j^{(n+1)} := x_j^{(n)} - \frac {z_j(x_j^{(n)})} {z_j(x_j^{(n)})}
= x_j^{(n)} + \frac {X - \sum_{i\in{}I} q_{j,i} (x_j^{(n)})^{p_{j,i}}}
                    {\sum_{i\in{}I} q_{j,i} p_{j,i} (x_j^{(n)})^{p_{j,i} - 1}}$$

The other solutions may be found then by adjustment of the starting value $0 < x_j^{(0)} < X$.

Alternatively, a solution may be found with the fixed-point iteration method

$$x_j^{(n+1)} := X - \sum_{i\in{}I \land i\ne j} q_{j,i} (x_j^{(n)}))^{p_{j,i}}$$

To be fair, all these numerical methods are not of great use without the qualitative analysis.
But there is one relatively simple special case, which may be easily analyzed.

**** Homogeneity.

If $\alpha$ is not only diagonal, but additionally $\sigma_k$ doesn't depend on $k$
(i.e. the production function is homogeneous), then there are two possibilities.

- If $\sigma_k = 1$, then the objective $Y$ is linear ($y_k = A_k x_k$),
  and we have the simplest form of /linear programming/.
  The system is then

  $$A_i = \frac {\sum_{k\in{}I} y_k} {X}$$

  Here all $x$ are stationary, if all $A_i$ are equal, or none otherwise.
  Either way, the optimum is on the boundary, more precisely on the vertices,
  when $x_k = 0$ for all but one $k$. [fn:glpk]

- If $\sigma_k \neq 1$, then

  $$\sum_{i\in{}I} \left( \frac {A_j} {A_i} \right)^{\frac 1 {\sigma_i - 1}} x_j = X$$

  That is, $x$ has a uniquely defined stationary point: which is the /minimum/,
  if $\sigma_k > 1$, then the maximum is again in a vertex; or is the maximum, if $\sigma_k < 1$.
  (No proof, but it is easy to see when $|I| = 2$.)

When $\sigma_k \ge 1$, this analytic framework has nothing useful to say.
Otherwise, when $\sigma_k < 1$, in the optimum the production function looks like

$$y_k = A_k \left( \frac {X} {\sum_{i\in{}I} (A_k / A_i)^{\frac 1 {\sigma_i - 1}}} \right)^{\sigma_k}
      = \frac {(A_k)^{\frac 1 {1 - \sigma_k}}}
              {\left( \sum_{i\in{}I} (A_i)^{\frac 1 {1 - \sigma_k}} \right)^{\sigma_k}}  X^{\sigma_k}
$$

It doesn't contain the entropic component. The total value is $Y = K X^{\sigma_k}$, for a constant $K$.
And the evolution is described above (single good case): it will stabilize, approaching a certain value $X$.

Finally, the P.P.F., as a constraint, in this case is an "ellipse"
in a Lebesgue (normed vector) space $L^{\sigma_k}$.
Sphere, if all $A_k$ are the same, and a ball, if we haven't yet excluded the suboptimal points.

[fn:glpk] GNU Linear Programming Kit and MetaProg Modelling Language are
simple but efficient tools to solve problems of this kind, including much more sophisticated.

** Discussion.

It's easy to notice that the complexity of the derivations increases drastically,
as we come from the simplest, or even trivial, cases to the more practical extensions of the models.
In fact, almost any practically useful version of a model of this kind is intractable by the simple analytic tools,
and requires much more esoteric techniques to find a more or less concrete answers to the (easy) questions.
That's probably the reason why practitioners (economists) more often use a vague pictorial descriptions
and hand-waving than the much more exact (mathematical) formulations -- the math is hard,
and the learning curve is steep, compared to, say, mechanics.

It's probably also worth noting that mathematical models in general admit variations,
there are no "right" and "wrong"; a la -- all models are wrong, but some are useful.
Be it a model, data, or a scoring system: it's better to have one,
even if it's far from being exact, than always rely on intuition.

So, if a more exact version of a model doesn't (yet) produce an answer,
it's better to introduce some additional assumptions,
than drop the model altogether, preferring pictorial description and appeal to intuition instead.
It's all about precision vs. tractability trade-off -- not about applicability of math itself.

* Microeconomics.

In most of the courses of economics, a large part is devoted to the laws of demand and supply,
to tell how important they are, and how they may be used to explain many phenomena.
And yet, they never tell a single /algebraic formulation/ for them:
all the theory is explained in terms of "increase/decrease", "shifts" etc.
Leaving no hope for being able to /compute/ anything.

With the expression for production function, a quite concrete expressions for the laws may be derived,
assuming each agent uses the resources the most efficient way (sure, that's actually not quite the case,
but it's just nit-picking, cause it's true for any model in any subject).

** Prices, demand and supply.

Let the coefficients $A$ in an agent's production function have the form

$$A_k := p_k B_k \text{ and also } y_k := p_k q_k$$

where $q$ are the quantities of the goods produced, in their own units,
$p$ are the prices per unit, and $B$ -- some other proportionality coefficients.
So that $Y := \sum_{k\in{}I} y_k = \sum_{k\in{}I} p_k q_k$ is the quantity we optimized.

Let also assume the settings as in the evolution problem above, in particular, $X_{n+1} = Y_n$.
I.e., that after the $n$-th period, it was produced the amount $q_{n,k}$ of each good,
and sold for the price $p_{n,k}$ respectively. So the budget for the next cycle is

$$X_{n+1} = Y_n = \sum_{k\in{}I} y_{n,k} = \sum_{k\in{}I} p_{n,k} q_{n,k}$$

----------------

When the agent's production function has rank $\alpha \le 1$,
and the available budget $X$ is utilized the most efficient way,
the dependence of the output(s) on the budget is

$$y_{n,k} = K_{n,k} X_n^{\sigma_{n,k}} \text{ or }
  q_{n,k} = \frac {K_{n,k}} {p_{n,k}} X_n^{\sigma_{n,k}}$$

where coefficients $K$, $p$ and $\sigma$ do not depend on $X$ itself.
So the amounts of production $q$ are dependent on the available budget $X$ as the power function(s).
The output by the end of the $n+1$-th cycle is

$$q_{n+1,k} = C_{n+1,k} \left( \sum_{i\in{}I} p_{n,i} q_{n,i} \right)^{\sigma_{n+1,k}}
 \text{ where } C_{n+1,k} := \frac {K_{n+1,k}} {p_{n+1,k}}$$

This may be viewed as the "Law of Supply" of a single agent.
In the sense that it describes how much of the goods the agent may produce, given the price,
for which it might sell the output of the previous cycle, to get the budget for the next one;
and supposing all the other parameters are the same.
By price here is meant just a proportionality coefficient.

In particular, when the agent produces just one good, $|I|=1$,

$$q_{n+1} = \left( \frac {K_{n+1}} {p_{n+1}} q_{n}^{\sigma_{n+1}} \right)  p_{n}^{\sigma_{n+1}}$$

More generally, and disregarding the time-dependence,
it may be a function $S: {\mathbb{R}_+}^I \to {\mathbb{R}_+}^I$ of the form

$$S_k(p) := C_k \left( \sum_{i\in{}I} p_i q_i \right)^{\sigma_k}$$

for some coefficients $C$.

----------

Analogously we may derive the "Law of Demand", of an individual agent.

Let the inputs be defined via $x_k := p_k q_k$, where $q$ are the quantities of the resources consumed,
in their own units, and $p$ are the prices per unit.
So that $X := \sum_{k\in{}I} x_k = \sum_{k\in{}I} p_k q_k$ is the equality constrain.

When again the agent's production function has rank $\alpha \le 1$,
and the available budget $X$ is utilized the most efficient way,
the dependence of the input(s) on the budget is

$$p_i q_i = x_i = X \rho_i$$

So the dependence is simply an inverse proportionality.

*** Determination.

Now, since "the laws" have a precise algebraic expressions we may find the parameters,
specific to the production/agent, by means of /regression analysis/ on a data-set.
I.e. if we have a set of pairs of prices and corresponding amounts of production or consumption,
assuming all the other factors are the same (all the hidden variables are fixed),
we may just fit a power or inverse proportion function into the set.

And then use these concrete "laws", expressions with the instantiated coefficients, for some predictions.

These data may be gathered by addition of a small /random variation/ to the /current price/.
Since the prices are chosen to some extent /arbitrarily/ (by the agents /individually/),
making the price slightly /fluctuating/ may produce the vital data
on the form of the curves in the neighborhood of the current price.
(So a buyer/seller may determine to what extent the currently set price is optimal.)

*** Non-linearity.

We might also bring into consideration possible dependence of the price per unit on the volume.
And let, say, $y_k := p_k q_k^{\mu_k}$ and $x_i := p_i q_i^{\nu_i}$, for some $\mu,\nu: {\mathbb{R}_+}^I$,
as a form of non-linearity. It wouldn't however dramatically change "The Laws".
There are also some other options, which would; e.g. $x \mapsto p a \log (\frac x a)$.

** Markets and equilibria.

Now, suppose there are /multiple agents/. Say, $M$ is their index set.
And for every $m: M$, there are defined two functions $D_m, S_m: {\mathbb{R}_+}^I \to {\mathbb{R}_+}^I$,
sending the, presumably stable, prices into the demand and supply of an individual agent respectively.
Then we can define total demand and supply $\bar{D}, \bar{S}: {\mathbb{R}_+}^I \to {\mathbb{R}_+}^I$ by

$$\bar{D}(p) := \sum_{m\in{}M} D_m(p) \text{ and } \bar{S}(p) := \sum_{m\in{}M} S_m(p)$$

So if $D, S$ are somehow precisely defined, then we can formulate the equilibrium condition

$$\bar{D}(p) = \bar{S}(p) \text{ i.e. } \sum_{m\in{}M} D_m(p) = \sum_{m\in{}M} S_m(p)$$

And, if we managed to solve it for $p$, these are the equilibrial prices.

* Total production function.

In the multiple agents settings, we may also consider the total amount of resources consumed and goods produced
$\bar{x}, \bar{y}: {\mathbb{R}_+}^I$ defined as

$$\bar{y} := \sum_{m\in{}M} y_m \text{ and } y_m = f_m(x_m) \text{ and } \sum_{m\in{}M} x_m =: \bar{x}$$

where $x_m, y_m, f_m$ are the consumption, production, and production function of individual agents.
And define the total production function

$$\bar{y} = \sum_{m\in{}M} f_m(x_m) \text{ where } \sum_{m\in{}M} x_m = \bar{x}$$

Assuming $f_m$ to be of the most general form we considered so far (a power function), we can write

$$y_{m,k} := f_{m,k}(x_m) := A_{m,k} \prod_{i\in{}I} x_{m,i}^{\alpha_{m,k,i}}$$

And, with help of a measure,

$$y_{m,k} = A_{m,k} \prod_{i\in{}I} (\nu_{m,i} \bar{x}_i)^{\alpha_{m,k,i}}$$

where $x_{m,i} = \nu_{m,i} \bar{x}_i$, that $\sum_{m\in{}M} \nu_{m,i} = 1$ for all $i$. Then

$$y_{m,k} = A_{m,k} \prod_{i\in{}I} (\nu_{m,i}^{\alpha_{m,k,i}} \bar{x}_i^{\alpha_{m,k,i}})$$

$$y_{m,k} = \left( A_{m,k} \prod_{i\in{}I} \nu_{m,i}^{\alpha_{m,k,i}} \right)
  \prod_{i\in{}I} \bar{x}_i^{\alpha_{m,k,i}} = f_{m,k}(\nu_m) \prod_{i\in{}I} \bar{x}_i^{\alpha_{m,k,i}}$$

$$f_{m,k}(\nu_m) = A_{m,k} \prod_{i\in{}I} \nu_{m,i}^{\alpha_{m,k,i}}$$

Thus, for a fixed $\nu$, the outputs $y_{m,k}$ depend on $\bar{x}$ the same way as on $x_m$. Substitute,

$$\bar{y}_k = \sum_{m\in{}M} f_{m,k}(\nu_m) \prod_{i\in{}I} \bar{x}_i^{\alpha_{m,k,i}}$$

If the coefficients $\alpha_{m,k,i}$ actually do not depend on $m$,
then we can analyze the total production function as before, for each $\nu$.
Otherwise -- we've obtained yet more general form of a production function:

$$\boxed{f_k(x) = \sum_{m\in{}M} A_{m,k} \prod_{i\in{}I} x_i^{\alpha_{m,k,i}}}$$

Good news is that if we admit the individual agents may have production functions of this form,
not just a power functions, then the total production function will not exit this larger class.
So this form of a production function is, in this sense, closed.

This expression is a generalization of a polynomial, allowing non-integral powers.

* Continuous growth models.

The evolutions considered above are discrete-time, and hard to treat.
One way to simplify things is to employ differential equations as an approximation to the difference equations.
Among all the benefits they provide, a differential equation may be not chaotic,
while the corresponding difference equation is.

For example, one very general description of an evolution:
suppose $f: \mathbb{R} \to \mathbb{R}$ doesn't depend on time, and

$$\dot{x} = f(x)$$

is the differential equation, where $x$ denotes a state and $\dot{x}$ -- it's time-derivative, "rate of change".
Here the zeros of $f$ (such $x$ that $f(x)=0$) correspond to steady states;
and between them, where $f$ is positive/negative, $x$ increases/decreases over time,
approaching the steady states -- zeros of $f$.

Suppose then that $f(x) := a_0 + \cdots + a_n x^n$ is a polynomial of degree $n$ ($a_n \neq 0$).
And that $x = 0$ is a steady state, i.e. $a_0 = 0$. Let also all $a_i \ge 0$, for simplicity.
The solutions of this d.e., the dependence of the state $x$ on time, for different $n$ are

- for $n = 0$ -- constant function, every state is steady state
- for $n = 1$ -- exponential, a.k.a. Malthusian, growth model:
  single unstable steady states at $x = 0$, like for any higher $n$; growth everywhere else.
- for $n = 2$ -- logistic, a.k.a. S-, curve:
  two steady states, the other is stable; growth only between them.
- for $n = 3$ -- combination, in a sense, of $n = 2$ and $n = 1$:
  either single unstable $x = 0$, or one more semi-stable steady state for some $x > 0$; growth otherwise.
- for $n = 4$ -- "double" logistic curve: two steady states $x = 0$ and some $x > 0$,
  growth only in-between them; and maybe one more semi-stable steady state in the growth region.
- and so on... The point is that logistic curve is not the most general case.
  And that for $n > 2$ one possible scenario of evolution is: starting in $x = 0$;
  by virtue of a fluctuation escaping it, and approaching some other semi-stable state;
  staying there until another fluctuation happens, throwing the system into the next region.

By stability here is meant how the system responds to a /small perturbation/, when being in a steady state:
if it will return to it -- stable, if it will go away from it -- unstable,
if the response is dependent on the direction of the perturbation (the sign) -- call it semi-stable.

* Footnotes

[fn:up-index] ...it's the rare occasion where the tension might arise:
  between the upper (contravariant) indices of the coordinates, and their powers.
