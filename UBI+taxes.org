
#+TITLE: Universal/Unconditional Basic Income and the Taxes.

* Introduction.

Any real economy is neither purely capitalist nor socialist, it's a hybrid of these two:
they admit both, free market and social security.
Although during the course of history there were attempts to build a viable economy on just one view -- all failed.

Today, in connection with the impressive advances in Artificial Intelligence,
there is increasingly wide movement for reconsidering the proportion between free market and social security.
It is not the first time when people embrace a new technology and envision revolution,
which happened to be not quite as radical as thought before.
But indeed the point -- that people build machines not to work more for less wages -- makes sense.

* Universal/Unconditional Basic Income.

Here is the basic idea of UBI:
Significant part of the taxes, collected by the state, is evenly distributed among the citizens.
I.e. returned to the citizens, not in the form of a large directed (military primarily) projects,
or an again directed programs to support a groups deemed to be socially vulnerable (unemployed etc.);
but to *all* of the citizens *without a discrimination*,
including those who are totally able to be employed and do earn much more than the payments are.

*Note:* Although it may sound like a form of communism, this comparison is incorrect because the idea of communism,
at least as it was thought in Soviet Union, implied that free market is harmful,
therefore, not an acceptable option.

- So it was disallowed, thus rendering the system incapable for this sort of self-regulation.
  As the result, to maintain any form of life, the state had to resort to the most radical
  and unpopular means a state can apply to it's citizens. And that's what Soviet Union is infamous for.

- Conversely, UBI is proposed to co-exist with the free market
  and also some of the more traditional forms of social security, which it makes sense to add on top of UBI.
  So the potential for self-regulation is inhibited at least not more than in the currently dominating models.

In a most simple formulation. Let $n$ be the number of citizens, and $T$ be the total amount of taxes collected.
Then the amount $b$ of basic income paid to everyone is

$$b = \frac {\alpha T} n$$

Here $\alpha\in [0,1]$ is the part of the taxes returned in form of the basic income. Later we'll generalize it.

Let's do some of (informal) economic analysis. Suppose $\alpha$ and $n$ are fixed.
When only very few people are willing to earn above UBI, $T$ is small, then the amount $b$ is small too --
it is a stimulus for more people to earn more, to cover the needs for which the UBI is not enough.
When a lot of people earn a lot, and pay a lot of taxes, $T$ is big, then also the amount of UBI is big --
it is a stimulus for more people to not earn as much, cause UBI is enough for more things.\\
Therefore, modulo some inertia of the system, it will eventually stabilize near an equilibrium point;
maybe oscillating at first, but it should fade.
This equilibrium point is characterized by a certain amount of UBI $b_*$ and the budget $T_*$.

*Note:* The amount $b_*$ is /not guaranteed/ to cover all the needs of an individual,
as was promised by the communists; nor even the basic. But, thanks to free market allowed,
there is no excuse for introduction of any inhuman means of stimulation, as in S.U..

Closely related economic mechanism is /donation/. Like with social security the payments are /unconditional/.
The difference is that with social security the state collects the funds and decides to whom to send it.
While donations are distributed according to a sort of market mechanism.\\
That means the funds paid as a taxes may no longer be donated by the taxpayers to anyone they like the most.
In this sense these two mechanisms are mutually exclusive, and most likely there should be a balance between them.

Some concrete questions to ask here:

- What (equilibrial) amount of UBI $b_*$ is achievable and how?
- What part $\alpha$ of the taxes should be returned in form of UBI and why?

Also, an observation: any means of social security is a huge stimulus for /migration/.
In particular, since $b$ depends, among all the other factors, on the /population/ $n$:
the equilibrium may be reached not only by adjustment of the quantity and quality of
the work done for earning above UBI, but also simply by the adjustment of $n$, leaving $T$ untouched.\\
So that analysis is only applicable to a /closed/ system (but not necessarily a single country).
And UBI if implemented, as a byproduct, will likely create even higher barriers
for migration between the countries with different levels of UBI. This is a crucial aspect.

The other, broader but directly related questions:

- How to handle the migration? Isolated "ladder steps", whole-world government, other?
- What taxation schemes should be used to top up the budget?

* Taxation schemes.

Since social security benefits are after all paid from the taxes,
and UBI is nothing more than just a, maybe slightly radical, form of social security:
the taxation schemes themselves deserve close attention.
Here are considered some possible variations of them.
Although no direct connection to the UBI is discussed.

** Linear taxation.

In modern practice there are two alternative taxation schemes: the income tax and profit tax.
The difference between them is that ... the former is calculated on income and the latter is on profit.
Let $I$ be an income and $E$ an expenses, then $P = I - E$ is the profit.

$$T_P = \theta_P P = \theta_P I - \theta_P E$$
$$T_I = \theta_I I = \theta_I I - 0 E$$

The state may offer a taxpayer the choice: whether the amount of tax is calculated on income or profit.
The income tax is set to be lower than the profit tax, $0 \le \theta_I < \theta_P \le 1$, so that:

- for larger expenses, relative to the income, the profit tax is more optimal, i.e. no profit - no tax;
- for smaller expenses, the income tax is more optimal, because the rate is lower.

Actually there is one point on the scale of income to expenses ratio, separating these two regions.

The trick is that the lower ratio $I:E$ is usually accompanied by brute-force business models, monotone labor etc.
While higher incomes, relative to the expenses, are often a sign of more clever businesses, read innovations.
So the profit tax is maintained for those whose profit is less than the income tax would be,
and the income tax exists as a means of stimulation for innovations.\\
Well, and also simply because it is easier to check the income of a firm alone than also the expenses.

These two are actually only a limiting cases of a more general pattern. There may be more gradations for the rates.
A more general (linear) taxation scheme may be of the form:

$$T = \theta_{+} I - \theta_{-} E$$

Where $0 \le \theta_{-} \le \theta_{+} \le 1$.

In particular, when $\theta_- = 0$, we obtain the income tax, $T = \theta_+ I - 0 E$;
and when $\theta_- = \theta_+$, the profit tax, $T = \theta_+ I - \theta_+ E$.

Suppose now $I$ and $E$ are fixed, and we have two such general taxation schemes to choose from:

$$T = \theta_+ I - \theta_- E$$
$$T' = \theta_+' I - \theta_-' E$$

Consider the difference

$$T - T' = (\theta_+ I - \theta_{-} E) - (\theta'_+ I - \theta'_{-} E)$$
$$T - T' = (\theta_+ - \theta'_+) I - (\theta_{-} - \theta'_{-}) E$$

The separation point $T = T'$ is given by

$$(\theta_+ - \theta_+') I = (\theta_- - \theta_-') E$$

Since $I, E \ge 0$,
it only makes sense to have the choice if

$$\theta_+ \lessgtr \theta_+' \iff \theta_- \lessgtr \theta_-'$$

And if it's true, then there is a single known separation point.

$$\frac I E = \frac {\theta_- - \theta_-'} {\theta_+ - \theta_+'}$$

In particular, for the income vs. profit tax case,
$\theta_+ = \theta_- = \theta_P$ and $\theta_+' = \theta_I$, $\theta_-' = 0$, it is

$$\frac I E = \frac {\theta_P} {\theta_P - \theta_I}$$

This principle may be extended onto a larger set of the schemes of this kind (linear):
by defining $\theta_+, \theta_-: A \to \mathbb{I}$ for some set $A$,
and $\forall a, b\in A: \theta_+(a) < \theta_+(b) \iff \theta_-(a) < \theta_-(b)$;
this is to ensure that for every pair of the schemes having the choice makes sense.\\
The effect of having this choice is that higher the income (for a fixed expenses) of a firm is --
lower is the tax rate accessible for it.

** Non-linearity.

Suppose $X$ is the "raw" profit or income of a firm, i.e. without the tax,
and $\theta \in [0,1]$ is the linear tax rate. Then the amount of tax to pay is $\theta X$,
and the remaining "dry" profit, respectively income, $Y$ is given by

$$Y = (1 - \theta) X$$

So the part of the profit/income paid as the tax is not depending on the amount. Is this Okay?\\
Perhaps it is better if the tax was lower for smaller amounts of $X$ and higher for bigger $X$:
leaving alone those who aren't yet well adapted to the markets, encouraging more people to start and grow,
fostering new players; and placing more fiscal responsibilities on those who already have a strong positions,
call them "adults" ...
I expect most people agree that differential taxation has many advantages: decentralization, diversity etc.
So how can it be implemented? Logarithmic scale.

$$\frac Y a = (1 - \theta) \log \left( \frac X a + 1 \right)$$

Here $a > 0$ is another parameter.
Since Taylor expansion of $\log(x)$ near $1$ is

$$\log(x) = \sum_{k=1}^{\infty} \frac {(-1)^{k-1}} k (x - 1)^k = (x - 1) - \frac {(x - 1)^2} 2 + \cdots$$

$$\log(x + 1) = \sum_{k=1}^{\infty} \frac {(-1)^{k-1}} k x^k = x - \frac {x^2} 2 + \cdots$$

A series expansion for $Y$ is

$$\frac Y a = (1 - \theta) \sum_{k=1}^{\infty} \frac {(-1)^{k-1}} k \left(\frac X a\right)^k$$

$$Y = (1 - \theta) \left( X - \frac {X^2} {2 a} + \cdots \right)$$

And when $a\to\infty$ we obtain the former proportional (linear) tax model.

This new non-linear taxation scheme has one more parameter, $a$, in addition to $\theta$.
But we might drop one by choosing $\theta = 0$, meaning that when the profit/income is low,
the firm is practically free from the tax, and the amount of tax burden is varied only with $a$.

$$Y = a \log \left( \frac X a + 1 \right)$$

So the parameter $a$ is determining at what point the tax is reaching a given size.

\includegraphics[page=1]{log(x+1)}

\includegraphics[page=2]{log(x+1)}

** The combination.

The logarithmic version of the income and profit tax, combined with the general form of linear tax,
yields different expressions for the "dry" profit.

First, reason as if $X$ was the income. Let the "dry" profit $P'$ be

$$P' = P - T = (I - E) - (\theta_+ I - \theta_- E)$$
$$P' = (1 - \theta_+) I - (1 - \theta_-) E = I' - E'$$

And "dry" income $I'$ and expenses $E'$ respectively

$$I' = (1 - \theta_+) I \text{ and } E' = (1 - \theta_-) E$$

We could apply the non-linearity to both of these

$$\frac {I'} {a_+} = (1 - \theta_+) \log \left( \frac I {a_+} + 1 \right)$$
$$\frac {E'} {a_-} = (1 - \theta_-) \log \left( \frac E {a_-} + 1 \right)$$

Then "dry" profit would be

$$P' = (1 - \theta_+) a_+ \log \left( \frac I {a_+} + 1 \right) -
       (1 - \theta_-) a_- \log \left( \frac E {a_-} + 1 \right)$$

Then as earlier assume there are two taxation schemes;
so that "dry" profit (for each of the schemes), now denoted by $P$ and $P'$ respectively, is given by

$$P = (1 - \theta_+) a_+ \log \left( \frac I {a_+} + 1 \right) -
      (1 - \theta_-) a_- \log \left( \frac E {a_-} + 1 \right)$$
$$P' = (1 - \theta_+') a_+' \log \left( \frac I {a_+'} + 1 \right) -
       (1 - \theta_-') a_-' \log \left( \frac E {a_-'} + 1 \right)$$

Compare them as earlier, but the general formula for $P - P'$ is too big and intractable.
Therefore consider a special case: when $a_+ = a_+'$ and $a_- = a_-'$

\[P - P' = \left( (\theta_+' - \theta_+) a_+ \log \left( \frac I {a_+} + 1 \right) \right)  -
           \left( (\theta_-' - \theta_-) a_- \log \left( \frac E {a_-} + 1 \right) \right)\]

Then $P = P'$ when

\[(\theta_+' - \theta_+) a_+ \log \left( \frac I {a_+} + 1 \right) =
  (\theta_-' - \theta_-) a_- \log \left( \frac E {a_-} + 1 \right)\]

This is not much more complex than the linear taxation scheme.
Here we may also be sure that there is single separation point,
given the same conditions for $\theta$, and may generalize it to the case of having multiple choices.

Actually the linear scheme may be obtained when $a_+, a_-\to\infty$.
When only $a_-\to\infty$, it is the non-linear income tax,
where $X = I$ and $Y = I'$ are "raw" and "dry" incomes.

By letting $b_+ := (1 - \theta_+) a_+$ and $b_- := (1 - \theta_-) a_-$, we could also write

$$\frac {I'} {b_+} = \log \left( \frac {(1 - \theta_+) I} {b_+} + 1 \right)$$
$$\frac {E'} {b_-} = \log \left( \frac {(1 - \theta_-) E} {b_-} + 1 \right)$$

$$P' = b_+ \log \left( \frac {(1 - \theta_+) I} {b_+} + 1 \right) -
       b_- \log \left( \frac {(1 - \theta_-) E} {b_-} + 1 \right)$$

But it's of not much use. Alternatively, we could apply the non-linearity to the profit tax.

$$\frac {P'} a = \log \left( \frac {(1 - \theta_+) I - (1 - \theta_-) E} a + 1 \right)$$

This is also converging to the linear scheme, when $a\to\infty$, but these two extensions are incompatible.
The main question here is what reward method is more stimulating (and fir?): it's all about the policies.

* Differential UBI.

Back to the theory of basic income.
Let us now clarify and extend that simplistic model considered at the beginning.
First define some general criteria of "fairness": when the budget is larger, /everyone/ receives more,
and larger the budget is -- less are the relative /differences/ between the individuals' amounts.

Let $C$ be a set of citizens, $n = |C|$,
and let the amount of basic income $x: C \to \mathbb{R}_+$ be different for individuals.
So that

$$\sum_{i\in C} x_i \le T$$

Here $T$ is as earlier total amount of taxes collected. Obviously $T\to 0\implies x_i\to 0$ for all $i$.
And we also require that $x_i$ are strictly monotonic on $T$,
unbounded, i.e. when $T\to\infty\implies x_i\to\infty$, and (the key feature of UBI)

$$\boxed{\lim_{T\to\infty} \frac {x_i} {x_j} = 1}$$

for all $i, j \in C$ (all the $\infty$'s here are positive).

** Linear law.

Suppose $x$ are polynomials on $T$,

$$x_i = k_{i,0} + k_{i,1} T + \cdots + k_{i,p_i - 1} T^{p_i - 1} + k_{i,p_i} T^{p_i}$$

here $k_{i,{p_i}} \neq 0$ (actually $k_{i,{p_i}} > 0$, since $x_i \ge 0$ for all $T$).

For $T = 0$ it must be $x_i = 0$, then $k_{i,0} = 0$.
From the equivalence it follows that $p_i = p_j$, call it $m$, and $k_{i,m} = k_{j,m}$, call it a.

$$x_i = k_{i,1} T + \cdots + k_{i,m} T^m$$
$$x_i = (k_{i,1} + \cdots + a T^{m-1}) T$$

- When $m = 1$,

  $$x_i = a T$$
  $$\sum_{i\in C} x_i = \sum_{i\in C} a T = n a T \le T$$
  $$n a \le 1 \text{ i.e. } a \le 1 / n$$
  
  This is the model used to illustrate the idea of UBI, $a = \alpha / n$, $\alpha\in [0,1]$.

- When $m = 2$, the payments may be actually differential.
  Let $b_i := k_{i,1}$, it must be $b_i \ge 0$ for monotonicity,

  $$x_i = (b_i + a T) T$$
  $$\sum_{i\in C} x_i = \sum_{i\in C} (b_i + a T) T \le T$$
  $$\left( \sum_{i\in C} b_i \right) + n a T \le 1$$

  When $a = 0$, then $\sum_{i\in C} b_i \le 1$, it is more or less the traditional social security system.
  The proportion $x_i / x_j = b_i / b_j$ doesn't depend on $T$, and not necessarily equals $1$.
  The case $m = 1$ may be obtained when $b_i$ doesn't depend on $i$.

  Introduce a measure $\rho: C \to \mathbb{I}$ (such that $\sum_{i\in C} \rho_i = 1$),
  determining the eligibility of an individual for the traditional social security benefits;
  and coefficient $\alpha: \mathbb{I}$, determining the whole amount spend on the social security:
  so that $b_i =: \alpha \rho_i$.

  $$x_i = \alpha \rho_i T$$
  $$\sum_{i\in C} x_i = \alpha \le T$$

  When $a > 0$,

  $$\alpha + n a T \le 1$$
  $$T \le \frac {1 - \alpha} {n a}$$

  So for any fixed parameters $a$, $n$, and $\alpha$,
  as $T$ reaches a certain value, the condition $\sum_{i\in C} x_i \le T$ may not be satisfied.
  I.e. non-trivial version of this dependence of $x$ on $T$ may only work for a region of small $T$.
  The same is true for polynomials of higher degrees, since the sum of $x_i$ grows faster than the bound.

So we may want to search for another expression for $x$, which work for the whole range of $T$.

** Logarithmic law.

Let $a, b: C \to \mathbb{R}_+$, and use the following expression for $x$

$$x_i = a_i \log (b_i T + 1)$$

It has the necessary properties: $x$ spans from $0$ to $\infty$ monotonically with $T$,
and since

$$\frac {d x_i} {d T} = \frac {a_i b_i} {b_i T + 1}$$

$$\lim \frac {x_i} {x_j} = \lim \frac {a_i \log (b_i T + 1)} {a_j \log (b_j T + 1)}
                         = \lim \frac {a_i b_i} {b_i T + 1} : \frac {a_j b_j} {b_j T + 1}
	                 = \lim \frac {a_i b_i} {a_j b_j} : \frac {b_i T + 1} {b_j T + 1}
		         = \frac {a_i b_i} {a_j b_j} : \frac {b_i} {b_j} = \frac {a_i} {a_j}$$

for $T\to\infty$. Therefore must be $a_i = a_j$, call it $c$. So

$$x_i = c \log (b_i T + 1)$$
$$\frac {d x_i} {d T} = c \frac {b_i} {b_i T + 1}$$

Since ${d x_i} / {d T} = c b_i$ for $T = 0$, and $x_i \le c b_i T$ for all $T$,
i.e. the curve lays below the tangent line at the origin. If

$$c \left( \sum_{i\in C} b_i \right) \le 1$$

then also

$$\sum_{i\in C} x_i \le c \left( \sum_{i\in C} b_i \right) T \le T$$

So it is a sufficient condition for

$$\sum_{i\in C} x_i = c \sum_{i\in C} \log (b_i T + 1)
                    = c \log \left( \prod_{i\in C} (b_i T + 1) \right) \le T$$

It is also necessary, because

$$\lim c \sum_{i\in C} \frac {\log (b_i T + 1)} T =
  c \sum_{i\in C} \lim \frac {b_i} {b_i T + 1} = c \sum_{i\in C} b_i$$

for $T\to 0$. And if $c \left( \sum_{i\in C} b_i \right) > 1$,
then for some small $T$ it will be $\sum_{i\in C} x_i > T$.

Finally, to parametrize all the allowed combinations of $b$ and $c$, let's introduce
a coefficients $\alpha: \mathbb{I}$, $\beta: \mathbb{R}_+$, and a measure $\rho: C\to\mathbb{I}$,
such that $b_i = \rho_i / \beta$ and $c = \alpha \beta$. then

$$\boxed{ \frac {x_i} \beta = \alpha \log \left( \rho_i \frac T \beta + 1 \right) }$$

Here, as above, $\alpha$ determines part of the whole budget spent, $\rho$ - the eligibility,
and $\beta$ is the "degree of non-linearity", because when $\beta \to \infty$, $x_i \to \alpha \rho_i T$,
i.e. reduces to the the linear law.

Note also that

$$x_i - x_j = c \log \left( \frac {b_i T + 1} {b_i T + 1} \right)$$

so

$$\lim_{T\to\infty} (x_i - x_j) = c \log \left( \frac {b_i} {b_i} \right)$$

may be not equal to $0$.
It means that even despite the absolute difference in the benefits is always present,
if $b_i \neq b_j$, but the relative difference should make it unimportant.

* Discussion.

Notice, how this formula, has the same expression as the formula for the taxation
(if we'd introduce differention measure for the tax).
Generally, taxes and social security benefits are dual concepts; so it is not surprising.
And the models we've considered so far include all the methods used in practice as a special cases (linear).
So the non-linear methods we've considered are a superset of those.
The reason, why these generalized models aren't used in practice,
is maybe because they're much more computationally intensive.
But it must not be a problem in the recent decades.
So it's probably time to reconsider practice of taxation and social security mechanisms,
in favour of these more accurate versions.

These two theories are still though pretty much disconnected.
To connect the theory of taxation schemes to the theory of UBI,
a more detailed assumptions about individuals' and firms' behavior are needed.
But even more or less precise description of the connection between the total amount $T$ of taxes collected
and the individuals' earnings is not quite straight-forward (because taxes are paid by firms, not persons).
