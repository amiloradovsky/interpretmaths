Interpretations of Mathematics.
================================

This is intended to be the one place where the sources of the writings
(from both of the blogs and some other yet unpublished notes) are stored,
and their transformations recorded.

The objective is to make the notes fit together,
to obtain any substantial body of the writings,
to organize the thoughts, and maybe obtain something valuable.

https://interpretmaths.blogger.com/